package cn.cl;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 17:00
 */
public class HelloServlce {
	HelloProperties helloProperties;

	public String sayHello(String name) {
		return helloProperties.getPrefix() + "-" + name + helloProperties.getSuffix();
	}

	public HelloProperties getHelloProperties() {
		return helloProperties;
	}

	public void setHelloProperties(HelloProperties helloProperties) {
		this.helloProperties = helloProperties;
	}
}
