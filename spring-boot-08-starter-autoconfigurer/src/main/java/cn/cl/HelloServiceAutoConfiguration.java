package cn.cl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 17:06
 */
@Configuration
@ConditionalOnWebApplication // web应用才生效
@EnableConfigurationProperties(HelloProperties.class)
public class HelloServiceAutoConfiguration {
	@Autowired
	HelloProperties helloProperties;

	@Bean
	public HelloServlce helloServlce() {
		HelloServlce helloServlce = new HelloServlce();
		helloServlce.setHelloProperties(helloProperties);
		return helloServlce;
	}
}
