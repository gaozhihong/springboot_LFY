package cn.cl;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 17:02
 */
@ConfigurationProperties(prefix = "hello")
public class HelloProperties {
	private String prefix;
	private String suffix;

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
}
