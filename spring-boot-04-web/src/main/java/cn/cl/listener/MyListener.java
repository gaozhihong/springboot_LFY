package cn.cl.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/29 - 15:18
 */
public class MyListener implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("启动");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("销毁");
	}
}
