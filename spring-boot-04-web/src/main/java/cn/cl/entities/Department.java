package cn.cl.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class Department {
	private Integer id;
	private String departmentName;

	public Department(Integer id, String departmentName) {
		this.id = id;
		this.departmentName = departmentName;
	}
}
