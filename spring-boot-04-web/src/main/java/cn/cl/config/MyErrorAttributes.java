package cn.cl.config;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/29 - 13:55
 */
@Component
public class MyErrorAttributes extends DefaultErrorAttributes {
	@Override
	public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
		Map<String, Object> map = super.getErrorAttributes(webRequest, includeStackTrace);
		map.put("company", "alibaba");
		Map<String, Object> ext = (Map<String, Object>) webRequest.getAttribute("ext", 0);
		map.put("ext", ext);
		return map;
	}
}
