package cn.cl.config;

import cn.cl.filter.MyFilter;
import cn.cl.listener.MyListener;
import cn.cl.servlet.Myservlet;
import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import javax.servlet.Servlet;
import java.util.Arrays;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/29 - 14:50
 */
@Configuration
public class MyServerConfig {
	// 注册三大组件
	@Bean
	public ServletRegistrationBean myServlet() {
		ServletRegistrationBean<Servlet> servletServletRegistrationBean = new ServletRegistrationBean<>(new Myservlet(), "/myServlet");
		return servletServletRegistrationBean;
	}

	@Bean
	public FilterRegistrationBean<Filter> myFilter() {
		FilterRegistrationBean<Filter> filterRegistrationBean = new FilterRegistrationBean();
		filterRegistrationBean.setFilter(new MyFilter());
		filterRegistrationBean.setUrlPatterns(Arrays.asList("/hello", "/myServlet"));
		return filterRegistrationBean;
	}

	@Bean
	public ServletListenerRegistrationBean myListener() {
		ServletListenerRegistrationBean servletListenerRegistrationBean = new ServletListenerRegistrationBean(new MyListener());
		return servletListenerRegistrationBean;
	}

	@Bean
	public WebServerFactoryCustomizer<ConfigurableWebServerFactory> webServerFactoryCustomizer() {
		return new WebServerFactoryCustomizer<ConfigurableWebServerFactory>() {
			@Override
			public void customize(ConfigurableWebServerFactory factory) {
				factory.setPort(8003);
			}
		};
	}
}
