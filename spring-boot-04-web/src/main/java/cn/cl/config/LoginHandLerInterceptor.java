package cn.cl.config;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 标题：登陆检查
 * 作者：何处是归程
 * 时间：2020/3/28 - 16:01
 */
public class LoginHandLerInterceptor implements HandlerInterceptor {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String username = (String) request.getSession().getAttribute("loginUser");
		if (StringUtils.isEmpty(username)) {
			// 未登录，返回登录页面
			request.setAttribute("message", "没有权限，请先登录");
			request.getRequestDispatcher("/").forward(request, response);
			return false;
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	}
}
