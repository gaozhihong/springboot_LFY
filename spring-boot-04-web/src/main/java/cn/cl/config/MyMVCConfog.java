package cn.cl.config;

import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/28 - 12:28
 */

/**
 * 使用WebMvcConfigurer可以来扩展springmvc的功能
 * 即保留了自动配置，也能用我们扩展的配置
 */
@Configuration
public class MyMVCConfog {

	@Bean
	public WebMvcConfigurer webMvcConfigurer() {
		WebMvcConfigurer webMvcConfigurer = new WebMvcConfigurer() {
			// 视图解析器
			@Override
			public void addViewControllers(ViewControllerRegistry registry) {
				registry.addViewController("/").setViewName("login");
				registry.addViewController("/index.html").setViewName("login");
				registry.addViewController("/main.html").setViewName("dashboard");
			}

//			// 注册拦截器
//			@Override
//			public void addInterceptors(InterceptorRegistry registry) {
//				// SpringBoot的拦截器不用处理静态资源
//				registry.addInterceptor(new LoginHandLerInterceptor())
//						.addPathPatterns("/**")
//						.excludePathPatterns("/", "/index.html", "/user/login");
//			}
		};
		return webMvcConfigurer;
	}

	@Bean
	public LocaleResolver localeResolver() {
		return new MyLocalResolver();
	}
}
