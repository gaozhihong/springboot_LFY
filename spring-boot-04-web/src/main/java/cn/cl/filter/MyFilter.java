package cn.cl.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/29 - 15:04
 */
public class MyFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		System.out.println("my filter process……");
		filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void destroy() {

	}
}
