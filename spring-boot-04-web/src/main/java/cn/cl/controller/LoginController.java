package cn.cl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/28 - 14:59
 */
@Controller
public class LoginController {
	@PostMapping("/user/login")
	public String login(@RequestParam("username") String username, @RequestParam("password") String password, Model model, HttpSession session) {
		if (!StringUtils.isEmpty(username) && "1".equals(password)) {
			session.setAttribute("loginUser", username);
			// 登录成功, 防止表单重复提交
			return "redirect:/main.html";
		} else {
			model.addAttribute("message", "用户名或用户密码错误");
			// 登录失败
			return "login";
		}
	}
}
