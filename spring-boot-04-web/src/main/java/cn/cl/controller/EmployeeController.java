package cn.cl.controller;

import cn.cl.dao.DepartmentDao;
import cn.cl.dao.EmployeeDao;
import cn.cl.entities.Department;
import cn.cl.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/28 - 16:44
 */
@Controller
public class EmployeeController {
	@Autowired
	EmployeeDao employeeDao;
	@Autowired
	DepartmentDao departmentDao;

	/**
	 * 查询所有员工返回列表页面
	 */
	@GetMapping("/emps")
	public String list(Model model) {
		Collection<Employee> employees = employeeDao.getAll();
		// 放在请求域中
		model.addAttribute("emps", employees);
		return "emp/list";
	}

	/**
	 * @功能: 跳转添加员工界面
	 * @作者: 高志红
	 */
	@GetMapping("/emp")
	public String toAddPage(Model model) {
		// 来到添加页面，查出所有部门，在页面显示
		Collection<Department> departments = departmentDao.getDepartments();
		model.addAttribute("depts", departments);
		return "emp/add";
	}

	/**
	 * @功能: 员工添加
	 * @作者: 高志红
	 * springMVC自动将请求参数和入参对象的属性进行一一绑定，要求请求参数的名字和javaBean入参的对象里面的的属性名是一样的
	 */
	@PostMapping("/emp")
	public String addEmp(Employee employee) {
		System.out.println("保存的员工数据为：" + employee);
		// 来到员工列表页面
		// 保存员工
		employeeDao.save(employee);
		// redirect：表示重定向一个地址
		// forward：表示转发到一个地址
		return "redirect:/emps";
	}

	/**
	 * @功能: 跳转修改员工界面
	 * @作者: 高志红
	 */
	@GetMapping("/emp/{id}")
	public String toEditPage(@PathVariable("id") Integer id, Model model) {
		Collection<Department> departments = departmentDao.getDepartments();
		model.addAttribute("depts", departments);
		Employee employee = employeeDao.get(id);
		model.addAttribute("emp", employee);
		// 回到修改页面（add是一个修改添加二合一的一个页面）
		return "emp/add";
	}

	/**
	 * @功能: 修改员工
	 * @作者: 高志红
	 */
	@PutMapping("/emp")
	public String editEmp(Employee employee) {
		System.out.println("修改的员工数据" + employee);
		employeeDao.save(employee);
		return "redirect:/emps";
	}

	/**
	 * @功能: 删除员工
	 * @作者: 高志红
	 */
	@DeleteMapping("/emp/{id}")
	public String delete(@PathVariable("id") Integer id) {
		employeeDao.delete(id);
		return "redirect:/emps";
	}
}
