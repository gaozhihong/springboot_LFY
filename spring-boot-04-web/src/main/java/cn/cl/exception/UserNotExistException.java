package cn.cl.exception;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/29 - 13:14
 */
public class UserNotExistException extends RuntimeException {
	public UserNotExistException() {
		super("用户不存在");
	}
}
