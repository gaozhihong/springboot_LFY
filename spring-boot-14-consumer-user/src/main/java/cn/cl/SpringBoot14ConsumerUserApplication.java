package cn.cl;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 1、引入依赖
 * 2、配置dubbo的注册中心地址
 * 3、引用服务
 */
@EnableDubbo()
@DubboComponentScan()
@SpringBootApplication
public class SpringBoot14ConsumerUserApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBoot14ConsumerUserApplication.class, args);
	}
}
