package cn.cl.service;

import cn.cl.ticketservice.TicketService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;


@Service
public class UserService {
	@Reference
	TicketService ticketService;

	public String hello() {
		System.out.println(ticketService);
		String ticket = ticketService.getTicket();
		System.out.println("买到" + ticket);
		return ticket;
	}
}
