package cn.cl.controller;

import cn.cl.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/3 - 15:03
 */
@RestController
public class UsreController {
	@Autowired
	UserService userService;

	@GetMapping("/user")
	public String user() {
		return userService.hello();
	}
}
