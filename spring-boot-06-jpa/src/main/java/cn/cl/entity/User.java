package cn.cl.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 13:37
 */
@Data
@ToString
// 使用JPA注解配置映射关系
@Entity // 告诉JPA这是一个实体类（和数据表映射的实体类）
@Table(name = "t_user") // @Table来指定和哪个数据表对应，如果省略默认表名就是类名小写
public class User {
	@Id //这是一个主键
	@GeneratedValue(strategy = GenerationType.IDENTITY) // 自增主键
	private Integer id;
	@Column(name = "last_name", length = 32) // 这是和数据表对应的一个列
	private String lastName;
	@Column // 省略的话列名就是属性名
	private String email;
}
