package cn.cl.controller;

import cn.cl.entity.User;
import cn.cl.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 13:57
 */
@RestController
public class UserController {
	@Autowired
	UserRepository userRepository;

	@GetMapping("/user/{id}")
	public User getUserById(@PathVariable("id") Integer id) {
		return userRepository.findById(id).get();
	}

	@GetMapping("/user")
	public User insertUser(User user) {
		User save = userRepository.save(user);
		return save;
	}
}
