package cn.cl.repository;

import cn.cl.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 13:49
 */

/**
 * 继承JpaRepository来完成对数据库的操作
 */
public interface UserRepository extends JpaRepository<User, Integer> {
}
