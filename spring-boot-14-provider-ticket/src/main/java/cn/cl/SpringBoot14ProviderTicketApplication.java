package cn.cl;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 1、 将服务提供者注册到注册中心
 *      引入dubbo和zkclient相关依赖
 *      配置dubbo的扫描包和注册中心地址
 *      使用@Service发布服务
 */
@EnableDubbo()
@DubboComponentScan()
@SpringBootApplication
public class SpringBoot14ProviderTicketApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBoot14ProviderTicketApplication.class, args);
	}
}
