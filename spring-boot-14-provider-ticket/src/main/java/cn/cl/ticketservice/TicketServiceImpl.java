package cn.cl.ticketservice;


import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

@Service
@Component
public class TicketServiceImpl implements TicketService {
	@Override
	public String getTicket() {
		return "《厉害了我的国》";
	}
}
