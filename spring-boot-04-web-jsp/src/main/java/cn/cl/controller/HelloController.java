package cn.cl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/30 - 10:07
 */
@Controller
public class HelloController {
	@GetMapping("/success")
	public String hello(Model model) {
		model.addAttribute("msg", "你好");
		return "success";
	}
}
