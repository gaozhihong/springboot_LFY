package cn.cl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot17ActuatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot17ActuatorApplication.class, args);
	}

}
