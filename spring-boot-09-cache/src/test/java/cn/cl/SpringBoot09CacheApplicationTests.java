package cn.cl;

import cn.cl.bean.Employee;
import cn.cl.mapper.EmployeeMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

@SpringBootTest
class SpringBoot09CacheApplicationTests {

	@Autowired
	EmployeeMapper employeeMapper;

	@Autowired
	StringRedisTemplate stringRedisTemplate; // k - v 都是字符串的

	@Autowired
	RedisTemplate redisTemplate; // k - v都是对象的

	@Autowired
	RedisTemplate<Object, Object> empRedisTemplate;

	@Test
	public void test1() {
		// stringRedisTemplate.opsForValue().append("msg", "java");
		// System.out.println(stringRedisTemplate.opsForValue().get("msg"));
		stringRedisTemplate.opsForList().leftPush("mylist", "大写");
	}

	@Test
	public void test2() {
//		Employee empById = employeeMapper.getEmpById(2);
//		empRedisTemplate.opsForValue().set("emp-02", empById);
		empRedisTemplate.opsForList().leftPush("list", "1");
		empRedisTemplate.opsForList().leftPush("list", "2");
		empRedisTemplate.opsForList().leftPush("list", "3");
	}

	@Test
	void contextLoads() {
		Employee employee = employeeMapper.getEmpById(1);
		System.out.println(employee);
	}

}
