package cn.cl.service;

import cn.cl.bean.Department;
import cn.cl.mapper.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.*;
import org.springframework.stereotype.Service;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 20:31
 */
@Service
@CacheConfig(cacheNames = "dept")
public class DepartmentService {
	@Autowired
	DepartmentMapper departmentMapper;

	@Cacheable(key = "#id")
	public Department getDeptById(Integer id) {
		System.out.println("查询部门号为" + id + "的部门");
		Department depertment = departmentMapper.getDepertmentById(id);
		return depertment;
	}
}
