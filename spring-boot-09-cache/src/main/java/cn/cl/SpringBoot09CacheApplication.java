package cn.cl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * 一、搭建基本环境
 * 1、导入数据库文件 创建出department和employee表
 * 2、创建JavaBean
 * 3、整合mybatis连接数据库
 * - ① 配置数据源信息
 * - ② 使用注解版mybatis
 * - - 1) @MapperScan指定需要扫描的Mapper接口所在的包
 *
 * 二、快速体验缓存
 * 步骤：
 * 1、开启基于注解的缓存 @EnableCaching
 * 2、标注缓存注解即可：@Cacheable、@CacheEvict、@CachePut
 *
 * 三、整合Redis作为缓存
 * 原理：CacheManager == Cache 缓存组件来实现给缓存中存取数据
 *      1、引入redis的starter，容器中保存的是RedisCacheManager
 *      2、RedisCacheManager帮我们创建RedisCatch来作为缓存组件，RedisCache通过操作redis缓存数据
 *      3、默认保存数据 k - v 都是Object， 利用序列化保存，如何保存为JSON？
 *          ① 引入了redis的starter，cacheManager变为RedisCacheManager;
 *          ② 默认创建的RedisCacheManager操作redis的时候使用的是RedisTemplate<Object, Object>
 *          ③ RedisTemplate<Object, Object>默认使用的是JDK的序列化机制
 *      4、自定义RedisCacheManager
 */
@EnableCaching
@MapperScan("cn.cl.mapper")
@SpringBootApplication
public class SpringBoot09CacheApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot09CacheApplication.class, args);
	}

}
