package cn.cl.bean;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 10:27
 */
@Data
@NoArgsConstructor
@ToString
public class Department {
	private Integer id;
	private String departmentName;
}
