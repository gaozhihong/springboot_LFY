package cn.cl.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/1 - 15:33
 */

public class MyCacheConfig {
	@Bean
	public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
		RedisTemplate<String, String> template = new RedisTemplate<>();
		template.setConnectionFactory(redisConnectionFactory);
		Jackson2JsonRedisSerializer<String> ser = new Jackson2JsonRedisSerializer<String>(String.class);
		template.setDefaultSerializer(ser);
		return template;
	}
}
