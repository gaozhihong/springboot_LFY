package cn.cl.controller;

import cn.cl.bean.Employee;
import cn.cl.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 20:35
 */
@RestController
public class EmployeeController {
	@Autowired
	EmployeeService employeeService;

	@GetMapping("/emp/{id}")
	public Employee getEmpById(@PathVariable("id") Integer id) {
		return employeeService.getEmpById(id);
	}

	@GetMapping("/emp")
	public Employee updateEmp(Employee employee) {
		Employee emp = employeeService.updateEmp(employee);
		return emp;
	}

	@GetMapping("/delemp")
	public String deleteEmp(Integer id) {
		employeeService.deleteEmp(id);
		return "success";
	}

	@GetMapping("/emp/lastName/{lastName}")
	public Employee getEmpByLastName(@PathVariable("lastName") String lastName) {
		return employeeService.getEmpByLastName(lastName);
	}
}
