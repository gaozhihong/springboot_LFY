package cn.cl.controller;

import cn.cl.bean.Department;
import cn.cl.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/1 - 19:17
 */
@RestController
public class DepartmentController {
	@Autowired
	DepartmentService departmentService;

	@GetMapping("/dept/{id}")
	public Department getDeparById(@PathVariable("id") Integer id) {
		return departmentService.getDeptById(id);
	}
}
