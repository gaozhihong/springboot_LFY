package cn.cl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class SpringBoot15SpringcloudEurekaServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBoot15SpringcloudEurekaServerApplication.class, args);
	}
}
