package cn.cl.controller;

import cn.cl.bean.Department;
import cn.cl.mapper.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 10:51
 */
@RestController
public class DeptController {
	@Autowired
	DepartmentMapper departmentMapper;

	@GetMapping("/dept/{id}")
	public Department getDepartment(@PathVariable("id") Integer id) {
		return departmentMapper.getDepertmentById(id);
	}

	@GetMapping("/dept")
	public Department insertDept(Department department) {
		departmentMapper.insertDept(department);
		return department;
	}
}
