package cn.cl.controller;

import cn.cl.bean.Employee;
import cn.cl.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 12:12
 */
@RestController
public class EmpController {
	@Autowired
	EmployeeMapper employeeMapper;

	@GetMapping("/emp/{id}")
	public Employee getEmp(@PathVariable("id") Integer id) {
		return employeeMapper.getEmpById(id);
	}
}
