package cn.cl.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 9:43
 */
@Configuration
public class DruidConfig {
	/**
	 * @功能: 配置数据源
	 * @作者: 高志红
	 */
	@ConfigurationProperties(prefix = "spring.datasource")
	@Bean
	public DataSource druid() {
		return new DruidDataSource();
	}


	// 配置Druid的监控
	// 1、配置一个管理后台的Servlet
	@Bean
	public ServletRegistrationBean statViewServlet() {
		ServletRegistrationBean<StatViewServlet> bean = new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");
		Map<String, String> initparams = new HashMap<>();
		initparams.put("loginUsername", "admin"); // 登录账号
		initparams.put("loginPassword", "admin"); // 登录密码
		initparams.put("allow", ""); // 允许谁访问
		initparams.put("deny", "192.168.15.21"); // 拒绝谁访问
		bean.setInitParameters(initparams);
		return bean;
	}

	// 2、配置一个web监控的filter
	@Bean
	public FilterRegistrationBean webStatFilter() {
		FilterRegistrationBean<Filter> bean = new FilterRegistrationBean<>();
		bean.setFilter(new WebStatFilter());
		Map<String, String> initparams = new HashMap<>();
		initparams.put("exclusions", "*.js,*.css,/druid/*"); // 配置白名单
		bean.setInitParameters(initparams);
		bean.setUrlPatterns(Arrays.asList("/*")); // 配置黑名单
		return bean;
	}
}
