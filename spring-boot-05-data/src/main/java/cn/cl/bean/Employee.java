package cn.cl.bean;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 10:25
 */
@Data
@NoArgsConstructor
@ToString
public class Employee {
	private Integer id;
	private String lastName;
	private Integer gender;
	private String email;
	private Integer dId;
}
