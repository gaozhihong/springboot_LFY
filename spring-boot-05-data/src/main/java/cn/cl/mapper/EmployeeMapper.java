package cn.cl.mapper;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 12:00
 */

import cn.cl.bean.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Mapper 或者 @MapperScan 将接口扫描装配到容器中
 */
@Mapper
public interface EmployeeMapper {
	public Employee getEmpById(Integer id);

	public void insertEmp(Employee employee);

}
