package cn.cl.mapper;

import cn.cl.bean.Department;
import org.apache.ibatis.annotations.*;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 10:29
 */
@Mapper
public interface DepartmentMapper {
	@Select("select * from department where id = #{id}")
	public Department getDepertmentById(Integer id);

	@Delete("delete from department where id = #{id}")
	public int deleteDeptById(Integer id);

	@Options(useGeneratedKeys = true, keyProperty = "id")
	@Insert("insert into department(departmentName) values(#{departmentName})")
	public int insertDept(Department department);

	@Update("update department set departmentName = ${departmentName} where id = ${id}")
	public int updateDept(Department department);
}
