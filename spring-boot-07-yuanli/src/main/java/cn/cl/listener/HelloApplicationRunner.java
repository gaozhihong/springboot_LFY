package cn.cl.listener;


import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 16:01
 */
@Component
public class HelloApplicationRunner implements ApplicationRunner {
	@Override
	public void run(ApplicationArguments args) throws Exception {
		System.out.println("HelloApplicationRunner");
	}
}
