package cn.cl.listener;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 15:51
 */
public class HelloApplicationContexInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
	@Override
	public void initialize(ConfigurableApplicationContext applicationContext) {
		System.out.println("测试ApplicationContextInitializer" + applicationContext);
	}
}
