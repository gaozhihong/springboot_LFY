package cn.cl.listener;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 16:03
 */
@Component
public class HelloCommandLineRunner implements CommandLineRunner {
	@Override
	public void run(String... args) throws Exception {
		System.out.println("HelloCommandLineRunner" + Arrays.asList(args));
	}
}
