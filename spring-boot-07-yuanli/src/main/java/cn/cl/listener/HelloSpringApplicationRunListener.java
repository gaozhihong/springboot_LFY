package cn.cl.listener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 15:53
 */
public class HelloSpringApplicationRunListener implements SpringApplicationRunListener {

	public HelloSpringApplicationRunListener(SpringApplication application, String[] args) {

	}

	@Override
	public void starting() {
		System.out.println("SpringApplicationRunListener启动了");
	}

	@Override
	public void environmentPrepared(ConfigurableEnvironment environment) {
		Object o = environment.getSystemProperties().get("os.name");
		System.out.println("SpringApplicationRunListener环境准备好了" + o);
	}

	@Override
	public void contextPrepared(ConfigurableApplicationContext context) {
		System.out.println("ConfigurableApplicationContext");
	}

	@Override
	public void contextLoaded(ConfigurableApplicationContext context) {
		System.out.println("ConfigurableApplicationContext加载完成");
	}

	@Override
	public void started(ConfigurableApplicationContext context) {

	}

	@Override
	public void running(ConfigurableApplicationContext context) {

	}

	@Override
	public void failed(ConfigurableApplicationContext context, Throwable exception) {

	}
}
