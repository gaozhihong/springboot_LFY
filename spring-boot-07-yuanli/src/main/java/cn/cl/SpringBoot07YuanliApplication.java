package cn.cl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot07YuanliApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot07YuanliApplication.class, args);
	}

}
