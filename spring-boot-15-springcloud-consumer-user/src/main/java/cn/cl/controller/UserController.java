package cn.cl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/3 - 16:50
 */
@RestController
public class UserController {
	@Autowired
	RestTemplate restTemplate;

	@GetMapping("/buy")
	public String byTicket(String name) {
		String ticket = restTemplate.getForObject("http://PROVIDER-TICKET/ticket", String.class);
		return name + "购买了" + ticket;
	}
}
