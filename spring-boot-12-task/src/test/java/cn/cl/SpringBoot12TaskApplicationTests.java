package cn.cl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@SpringBootTest
class SpringBoot12TaskApplicationTests {

	@Autowired
	JavaMailSenderImpl javaMailSender;

	/**
	 * @功能: 简单邮件
	 * @作者: 高志红
	 */
	@Test
	void contextLoads() {
		SimpleMailMessage message = new SimpleMailMessage();
		// 邮件标题
		message.setSubject("开学通知");
		// 邮件内容
		message.setText("2020年4月4日开学");
		// 发送给谁
		message.setTo("1821896956@qq.com");
		// 谁发的
		message.setFrom("1739745826@qq.com");
		javaMailSender.send(message);
	}

	/**
	 * @功能: 复杂邮件
	 * @作者: 高志红
	 */
	@Test
	void contextLoads2() throws MessagingException {
		// 创建一个复杂的消息邮件
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);
		// 邮件标题
		messageHelper.setSubject("开学通知");
		// 邮件内容
		messageHelper.setText("<h1 style='color:red'>2020年4月4日开学</h1>", true);
		// 发送给谁
		messageHelper.setTo("1821896956@qq.com");
		// 谁发的
		messageHelper.setFrom("1739745826@qq.com");
		// 上传文件
		messageHelper.addAttachment("1.jpg", new File("C:\\Users\\hcsgc\\OneDrive\\图片\\璐\\微信图片_20190801202320.jpg"));
		messageHelper.addAttachment("2.jpg", new File("C:\\Users\\hcsgc\\OneDrive\\图片\\璐\\微信图片_20200306172316.jpg"));
		javaMailSender.send(message);
	}
}
