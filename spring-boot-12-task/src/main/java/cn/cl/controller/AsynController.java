package cn.cl.controller;

import cn.cl.service.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/3 - 8:32
 */
@RestController
public class AsynController {
	@Autowired
	AsyncService asyncService;

	@GetMapping("/hello")
	public String hello() {
		asyncService.hello();
		return "success";
	}
}
