package cn.cl.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/3 - 8:40
 */
@Service
public class ScheduledService {
	/**
	 * second（秒）, minute（分）, hour（时）, day（天）, month（月）, week（周）.
	 */
	@Scheduled(cron = "0/3 * * * * *")
	public void hello() {
		System.out.println("hello");
	}
}
