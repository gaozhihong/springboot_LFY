package cn.cl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot02YmlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot02YmlApplication.class, args);
	}

}
