package cn.cl.bean;

import lombok.Data;
import lombok.ToString;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/27 - 10:29
 */
@Data
@ToString
public class Dog {
	private String name;
	private Integer age;
}
