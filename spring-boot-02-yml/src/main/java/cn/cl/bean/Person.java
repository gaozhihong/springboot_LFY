package cn.cl.bean;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/27 - 10:27
 */

/**
 * 将配置文件中的每一个属性的值映射到这个组件中
 * @ConfigurationProperties: 告诉SpringBoot将本类中所有属性和配置文件中相关的配置进行绑定
 * @Component: 将此类加入spring容器
 */
// @ConfigurationProperties(prefix = "person")
@Component
@Data
@ToString
public class Person {
	@Value("${person.name}")
	private String name;
	@Value("#{10 + 8}")
	private Integer age;
	@Value("${person.sex}")
	private Boolean sex;
	@Value("${person.birth}")
	private Date birth;
	private Map<String, Object> map;
	private List<Object> list;
	private Dog dog;
}
