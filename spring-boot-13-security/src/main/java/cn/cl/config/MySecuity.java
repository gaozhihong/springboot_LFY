package cn.cl.config;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/3 - 9:39
 */
@EnableWebSecurity
public class MySecuity extends WebSecurityConfigurerAdapter {
	/**
	 * @功能: 授权功能
	 * @作者: 高志红
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// 定制请求的授权规则
		http.authorizeRequests()
				.antMatchers("/").permitAll()
				.antMatchers("/level1/**").hasRole("VIP1")
				.antMatchers("/level2/**").hasRole("VIP2")
				.antMatchers("/level3/**").hasRole("VIP3");
		// 开启自动配置的登录功能
		// 1、login请求来到登录页
		// 2、重定向到/login?error表示登录失败
		// 3、可定制登录页
		// 4、一旦定制LoginPage：那么LoginPage的post请求就是登录
		http.formLogin().usernameParameter("username").passwordParameter("password").loginPage("/userlogin");
		// 开启自动配置的注销功能
		// 1、访问/logout表示用户注销，清空session
		// 2、注销成功就返回/login?logout页面
		http.logout().logoutSuccessUrl("/"); // 注销成功以后来到首页
		// 开启记住我功能
		// 登录成功以后将cookie发送给浏览器保存，以后登录带上这个cookie，只要通过检查就可以免登录， 点击注销就会删除这个cookie
		http.rememberMe().rememberMeParameter("remeber");
	}

	/**
	 * @功能: 认证功能
	 * @作者: 高志红
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())
				.withUser("cl").password(new BCryptPasswordEncoder().encode("123")).roles("VIP1", "VIP2")
				.and()
				.withUser("gzh").password(new BCryptPasswordEncoder().encode("123")).roles("VIP2", "VIP3")
				.and()
				.withUser("alsj").password(new BCryptPasswordEncoder().encode("123")).roles("VIP1", "VIP3");
	}
}
