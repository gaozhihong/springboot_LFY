package cn.cl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 1、引入security模块
 *      spring-boot-starter-security
 * 2、编写SpringSecurity的配置类
 *      @EnableWebSecurity
 *      public class MySecuity extends WebSecurityConfigurerAdapter { }
 * 3、控制请求的访问权限
 */

@SpringBootApplication
public class SpringBoot13SecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot13SecurityApplication.class, args);
	}

}
