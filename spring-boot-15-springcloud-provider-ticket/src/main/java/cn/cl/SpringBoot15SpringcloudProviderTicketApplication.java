package cn.cl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SpringBoot15SpringcloudProviderTicketApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBoot15SpringcloudProviderTicketApplication.class, args);
	}
}
