package cn.cl.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/1/11 - 22:22
 */
@RestController
public class HelloWorldController {
    @RequestMapping("/hello")
    public String hello() {
        return "<h1>Hello World</h1>";
    }
}
