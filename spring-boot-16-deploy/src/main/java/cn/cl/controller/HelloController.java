package cn.cl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/3 - 17:35
 */
@Controller
public class HelloController {

	@GetMapping("/")
	public String hello() {
		return "hello";
	}
}
