package cn.cl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot16DeployApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBoot16DeployApplication.class, args);
	}
}
