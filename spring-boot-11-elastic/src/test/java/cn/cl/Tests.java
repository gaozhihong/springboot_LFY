package cn.cl;


import cn.cl.bean.Book;
import cn.cl.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Tests {
	@Autowired
	BookRepository bookRepository;
	@Test
	public void test() {
		Book book = new Book();
		book.setId(1);
		book.setAuthor("志");
		book.setBookName("白鹿原");
		bookRepository.index(book);
	}
}
