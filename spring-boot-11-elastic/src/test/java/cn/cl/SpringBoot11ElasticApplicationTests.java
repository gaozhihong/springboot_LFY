package cn.cl;

import cn.cl.bean.Article;
import io.searchbox.client.JestClient;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

//@SpringBootTest
class SpringBoot11ElasticApplicationTests {

	@Autowired
	JestClient jestClient;

	/**
	 * @功能: 给ES中索引（保存）一个文档
	 * @作者: 高志红
	 */
	@Test
	void contextLoads() throws IOException {
		// 1、 给ES中索引（保存）一个文档
		Article article = new Article();
		article.setId(1);
		article.setTitle("好消息");
		article.setAuthor("高");
		article.setContent("Hello World");
		// 构建一个索引功能
		Index build = new Index.Builder(article).index("cl").type("news").build();
		// 执行
		jestClient.execute(build);
	}

	/**
	 * @功能: 搜索
	 * @作者: 高志红
	 */
	@Test
	public void search() throws IOException {
		String json = "{\n" +
				"    \"query\" : {\n" +
				"        \"match\" : {\n" +
				"            \"content\" : \"Hello\"\n" +
				"        }\n" +
				"    }\n" +
				"}";
		// 构建搜索功能
		Search build = new Search.Builder(json).addIndex("cl").addType("news").build();
		// 执行
		SearchResult searchResult = jestClient.execute(build);
		System.out.println(searchResult.getJsonString());
		// {"took":2,"timed_out":false,"_shards":{"total":5,"successful":5,"skipped":0,"failed":0},"hits":{"total":1,"max_score":0.2876821,"hits":[{"_index":"cl","_type":"news","_id":"1","_score":0.2876821,"_source":{"id":1,"author":"高","title":"好消息","content":"Hello World"}}]}}
	}
}
