package cn.cl.bean;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/2 - 19:49
 */
@Document(indexName = "lu", type = "book")
@Data
@ToString
@NoArgsConstructor
public class Book {
	private Integer id;
	private String bookName;
	private String author;
}
