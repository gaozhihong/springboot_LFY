package cn.cl.bean;

import io.searchbox.annotations.JestId;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/2 - 17:55
 */
@Data
@NoArgsConstructor
@ToString
public class Article {
	@JestId
	private Integer id;
	private String author;
	private String title;
	private String content;
}

