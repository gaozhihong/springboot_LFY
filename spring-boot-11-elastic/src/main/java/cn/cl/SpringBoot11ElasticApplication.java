package cn.cl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
SpringBoot默认支持两种技术和ES交互
1、Jest（默认不生效）
	需要导入jest的工具包：io.searchbox.client.JestClient
2、SpringBootData ElasticSearch
	① Client  节点信息clusterNodes clusterName
	② ElasticsearchTemplate 操作ES
	③ 编写一个ElasticsearchRepository的值接口来操作ES
 */
@SpringBootApplication
public class SpringBoot11ElasticApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBoot11ElasticApplication.class, args);
	}
}
