package cn.cl.repository;

import cn.cl.bean.Book;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/2 - 19:46
 */
public interface BookRepository extends ElasticsearchRepository<Book, Integer> { }
