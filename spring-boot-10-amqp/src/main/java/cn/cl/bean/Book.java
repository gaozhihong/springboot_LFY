package cn.cl.bean;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/2 - 9:42
 */

@Data
@NoArgsConstructor
@ToString
public class Book {
	private String bookName;
	private int prrce;

	public Book(String bookName, int prrce) {
		this.bookName = bookName;
		this.prrce = prrce;
	}
}
