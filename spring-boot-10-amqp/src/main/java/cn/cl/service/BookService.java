package cn.cl.service;

import cn.cl.bean.Book;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/4/2 - 9:56
 */
@Service
public class BookService {
	@RabbitListener(queues = "lu.news")
	public void receive(Book book, Message message) {
		System.out.println(message.getBody());
		System.out.println(message.getMessageProperties());
		System.out.println("收到消息" + book);
	}
}
