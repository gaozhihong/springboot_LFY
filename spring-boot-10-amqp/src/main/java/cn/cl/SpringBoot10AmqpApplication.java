package cn.cl;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
自动配置类：RabbitAutoConfiguration
	1、配置了连接工厂CachingConnectionFactory
	2、RabbitProperties封装了RabbitMQ的所有配置
	3、RabbitTemplate，给RabbitMQ发送和接收消息
	4、AmqpAdmin，RabbitMQ系统管理组件
		AmqpAdmin：创建和删除Queue，Exchange，Binding
	5、@EnableRabbit + @RabbitListener 监听消息队列的内容
 */
@EnableRabbit // 开启基于注解的RabbitMQ模式
@SpringBootApplication
public class SpringBoot10AmqpApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBoot10AmqpApplication.class, args);
	}
}
