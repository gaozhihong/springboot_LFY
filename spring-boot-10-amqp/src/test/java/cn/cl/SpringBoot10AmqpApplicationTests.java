package cn.cl;

import cn.cl.bean.Book;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringBoot10AmqpApplicationTests {

	@Autowired
	RabbitTemplate rabbitTemplate;

	@Autowired
	AmqpAdmin amqpAdmin;

	/**
	 * @功能: 单播 点对点
	 * @作者: 高志红
	 */
	@Test
	void direct() {
		// rabbitTemplate.send(exchage, routeKey, message); // 需要自己购在一个Message
		// rabbitTemplate.convertAndSend(exchage, routeKey, object); // 只需要传入要发送的对象，自动序列化发送给rabbitmq
		rabbitTemplate.convertAndSend("exchange.direct", "lu.news", new Book("西游记", 30));
	}

	/**
	 * @功能: 广播
	 * @作者: 高志红
	 */
	@Test
	public void fanout() {
		rabbitTemplate.convertAndSend("exchange.fanout", "", new Book("红楼梦", 49));
	}

	/**
	 * @功能: 接收数据
	 * @作者: 高志红
	 */
	@Test
	void receive() {
		Object object = rabbitTemplate.receiveAndConvert("lu.news");
		System.out.println(object.getClass());
		System.out.println(object);
	}

	/**
	 * @功能: 创建Exchange
	 * @作者: 高志红
	 */
	@Test
	public void createExchange() {
		amqpAdmin.declareExchange(new DirectExchange("amqpAdmin.exchange"));
		System.out.println("创建Exchange完成");
	}

	/**
	 * @功能: 创建Queue
	 * @作者: 高志红
	 */
	@Test
	public void createQueue() {
		amqpAdmin.declareQueue(new Queue("amqpAdmin.queue", true));
		System.out.println("创建Queue完成");
	}

	/**
	 * @功能: 创建Binding（绑定规则）
	 * @作者: 高志红
	 */
	@Test
	public void createBinding() {
		amqpAdmin.declareBinding(new Binding("amqpAdmin.queue", Binding.DestinationType.QUEUE, "amqpAdmin.exchange", "amqp.haha", null));
		System.out.println("创建Binding完成");
	}
}
