package cn.cl.controller;

import cn.cl.HelloServlce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 标题：
 * 作者：何处是归程
 * 时间：2020/3/31 - 17:22
 */
@RestController
public class HelloController {
	@Autowired
	HelloServlce helloServlce;

	@GetMapping("/hello")
	public String hello() {
		return helloServlce.sayHello("曹璐");
	}
}
